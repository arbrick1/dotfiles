#!/bin/sh


topDir=$(pwd)

srcFiles=("_bashrc"  "_dir_colors" "_vimrc" "nope")
dstFiles=(".profile" ".dir_colors" ".vimrc")

for (( i=0; i<${#srcFiles[@]}; i++ ));
do
	if [ ! -e ${srcFiles[$i]} ]; then
		echo "true"
	fi
done
