
" Setup Vundle
" REF https://github.com/gmarik/Vundle.vim
set nocompatible              " be iMproved, required
filetype off                  " required

" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
	" Let Vundle manage Vundle, required
	Plugin 'gmarik/Vundle.vim'
	" monokai colorscheme
   Plugin 'sickill/vim-monokai'
	" View manpages in vim
	Plugin 'jez/vim-superman'	
	Plugin 'scrooloose/nerdtree'
	Plugin 'vim-scripts/closetag.vim'
	Plugin 'Valloric/YouCompleteMe'
	Plugin 'edsono/vim-matchit'
call vundle#end()


" Determine auto indent settings based on filetype/contents, load plugins/indent files based on that
filetype indent plugin on
" Set colorscheme
" REF https://github.com/sickill/vim-monokai
colorscheme monokai
" Enable syntax highlighting
syntax enable
" Number of spaces per TAB
set tabstop=3
set shiftwidth=3
" Show line numbers
set number
" Show last command in bottom bar
set showcmd
" Highlight current line
" set cursorline
" Visual autocomplete for command menu
set wildmenu
" Redraw the screen only when needed (speeds up macros)
set lazyredraw
" Highlight matching paren
set showmatch
" Search as chars are entered
set incsearch
" Highlight search matches
set hlsearch
" Enable folding
set foldenable
" Open most folds by default
set foldlevelstart=10
" Set fold based on indent
set foldmethod=indent
" Move vertically by visual line
nnoremap j gj
nnoremap k gk
" Map <esc> to somewhere around the home row
inoremap jk <esc>
" Use case insensitive searching except when searching capital letters
set ignorecase
set smartcase
" Enable autoindent for files that have unknown filetypes
set autoindent
" Confirm exit with unsaved changes
set confirm
" Use visual queue to indicate error (rather than beep)
set visualbell
" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
" Enable auto indent
set ai
" Enable smart indent
set si
" Wrap lines
set wrap
" How many lines to force vim to keep in view when scrolling
set scrolloff=10
" Make paste work like it should
set pastetoggle=<F10>
inoremap <C-v> <F10><C-r>+<F10>
" View man pages in Vim (rather than less)
runtime ftplugin/man.vim
nnoremap K :Man <cword>
